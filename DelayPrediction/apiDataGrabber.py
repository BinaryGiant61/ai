import copy
import csv
from datetime import datetime
import requests
import json

# Documentation can be found at
#   https://wiki.openraildata.com/index.php/HSP

# When registering at https://datafeeds.nationalrail.co.uk/
# you only need the HSP subscription
# The Real time Data feed is too much to deal with
# The On Demand Data Feeds might be useful
#
# In 'Planned usage', mention you are using the HSP data
# for educational purposes, for a project, and for a limited
# time
# The T & Cs should not be an issue, nor the limit on the
# number of requests an hour - but do be polite and do not
# swamp the web service with an excessive number of requests

metrics_url = "https://hsp-prod.rockshore.net/api/v1/serviceMetrics"
details_url = "https://hsp-prod.rockshore.net/api/v1/serviceDetails"
headers = { "Content-Type": "application/json" }
auths = ('ajmalties@gmail.com', 'Hotline61!')
fromLoc= "NRW"

#data = {
#  "from_loc": fromLoc,
#  "to_loc": "LST",
#  "from_time": "0700",
#  "to_time": "0800",
#  "from_date": "2016-07-01",
#  "to_date": "2016-08-01",
#  "days": "SATURDAY",
#
#}
#
#r = requests.post(metrics_url, headers=headers, auth=auths, json=data)
#print(json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',',': ')))
#
#data["rid"] = "201607023389301"
#r = requests.post(details_url, headers=headers, auth=auths, json=data)
#print(json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',',': ')))

def grabServiceData(fromLoc,tooLoc,fromTime,tooTime,fromDate,toDate,Day):
    Servicedata = {
        "from_loc": fromLoc,
        "to_loc": tooLoc,
        "from_time": fromTime,
        "to_time":tooTime ,
        "from_date": fromDate,
        "to_date": toDate,
        "days": Day,

    }
    r = requests.post(metrics_url, headers=headers, auth=auths, json=Servicedata).json()
    processedServiceData ={}
    csvData=[]
    services =r['Services']
    for service in services:
        csvRow = [None] * 8
        csvRow[0] = Day

        currentMetrics = service.get('serviceAttributesMetrics')



        for rid in currentMetrics.get('rids'):
            data = Servicedata
            data["rid"]=rid
            csvRow[3]=rid
            date = requests.post(details_url, headers=headers, auth=auths, json=data).json()['serviceAttributesDetails']["date_of_service"]
            locations = requests.post(details_url, headers=headers, auth=auths, json=data).json()['serviceAttributesDetails']['locations']
            finalIndex=len(locations)
            csvRow[1] = date
            print("------------------------")
            print(rid)
            print("------------------------")

            if(rid == "202010016712172"):
               print("fresh meat !")
            canc= False
            finalIndex=getLastStationOnCancelled(locations)
            print("Final station : "+locations[finalIndex-1].get('location'))
            for i in  range(finalIndex-1):
                csvRow[2] = i+1
                startStation=locations[i]
                for j in range(i+1,finalIndex):

                    endStation=locations[j]
                    csvRow[4]=startStation.get('location')
                    csvRow[5]=endStation.get('location')
                    pta = datetime.strptime(endStation.get('gbtt_pta'), "%H%M")
                    ata= datetime.strptime(endStation.get('actual_ta'), "%H%M")
                    difference = (ata.hour*60+ata.minute) -(pta.hour*60+pta.minute)
                    csvRow[7]=difference


                    if (startStation.get('gbtt_ptd') == '' or startStation.get('actual_td')=='' ):
                        csvRow[6] =csvRow[7]
                    else:
                        ptd = datetime.strptime(startStation.get('gbtt_ptd'), "%H%M")
                        atd = datetime.strptime(startStation.get('actual_td'), "%H%M")
                        difference = (atd.hour * 60 + atd.minute) - (ptd.hour * 60 + ptd.minute)

                        csvRow[6] = difference
                   # print(startStation.get("location")+"->"+endStation.get("location"))

                    if(csvRow[6]>0):

                        temp = copy.deepcopy(csvRow)
                        csvData.append(temp)

    return csvData


def getLastStationOnCancelled(locations):
    carryOn = True
    i=0
    while carryOn and i<len(locations):
       current = locations[i]
       ptdPresent =current.get('gbtt_ptd') != ''
       atdPresent =current.get('actual_td') != ''
       ptaPresent =current.get('gbtt_pta') != ''
       ataPresent =current.get('actual_ta') != ''



       if((ataPresent==False and atdPresent==False)):
           carryOn=False
       else:
           i+=1
    return i




data = grabServiceData("NRW","LST","0800","0900","2020-08-01","2020-11-01","WEEKDAY")
print (data)
with open('hspData.csv',"w",newline="") as result_file:
    wr = csv.writer(result_file, dialect='excel')
    wr.writerows(data)