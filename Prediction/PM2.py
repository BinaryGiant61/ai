import pandas as pd
import sklearn as sklearn
import numpy as np
pd.options.mode.chained_assignment = None
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import pickle


# holds the current data set which is needed to scaling with new data
data_copy = None

# points to the location of the training data of historic train tables
# this data has already been processed to be purposed for prediction of delay
def build_data_set(file):
	training_data = pd.read_csv(file)
	data = training_data[['day', 'time', 'stop_num', 'rid', 'start', 'end', 'init_delay', 'end_delay']]

	return data

def accuracy(confusion_matrix):
	diagonal_sum = confusion_matrix.trace()
	sum_of_all_elements = confusion_matrix.sum()
	
	
	return diagonal_sum / sum_of_all_elements
def get_delay_range(delay):
	delay = int(delay)
	
	if delay <= 0: 
		return 0
	elif delay > 0 and delay <= 5:
		return 1
	elif delay > 5 and delay <= 10:
		return 2
	elif delay > 10 and delay <= 15:
		return 3
	elif delay > 15 and delay <= 20:
		return 4
	elif delay > 20 and delay <= 25:
		return 5
	elif delay > 25 and delay <= 30:
		return 6
	else:
		return 7

def get_time(time):
	if time < 12:
		return 'MORNING'
	if time >= 12 and time < 18:
		return 'AFTERNOON'
	if time > 18 and time <= 21:
		return 'EVENING'

# returns the required classifier in order to classify train delay for the journey
def init():
	global data_copy
	# load in the data
	data = build_data_set("hspData2.csv")

	#---------------------------------------------------------------
	# process data to ensure labels for specific delay ranges
	# 0 = no delay (on earlier)
	# 1 = 1-3 min delay
	# 2 = 4-5 min delay
	# 3 = 6-10 min delay
	# 4 = 11-14 min delay
	#5 = 15-17 min delay
	# 6 = 18-20 min delay
	# 7 = 20+ min delay 
	count = 0
	for datum in data['end_delay']:

		data['end_delay'][count] = get_delay_range(datum)
		data['time'][count] = get_time(datum)
		
		count = count + 1

	save_data(data)
	#---------------------------------------------------------------
	# setting up the data: label encoding

	print(data)

	encode = LabelEncoder()
	data['day'] = encode.fit_transform(data['day'])
	data['stop_num'] = encode.fit_transform(data['stop_num'])
	data['rid'] = encode.fit_transform(data['rid'])
	data['start'] = encode.fit_transform(data['start'])
	data['end'] = encode.fit_transform(data['end'])
	data['time'] = encode.fit_transform(data['time'])
	#---------------------------------------------------------------
	# setting up the data: feature scaling
	feature_scaler = StandardScaler()
	data[['day', 'stop_num', 'rid', 'start', 'end', 'time', 'init_delay']] = feature_scaler.fit_transform(data[['day', 'stop_num', 'rid', 'start', 'end', 'time', 'init_delay']])
	#---------------------------------------------------------------	
	# building training data and validation data
	# training data will be used to train the initial model
	# validation data will be used to test whether the model performs as expected

	training, validation = train_test_split(data, test_size = 0.3, random_state = 21)


	# training values
	train = (training.iloc[:,0:-1].values, training.iloc[:,-1].values)
	validate = (validation.iloc[:,0:-1].values, validation.iloc[:,-1].values)
	#---------------------------------------------------------------

	
	from sklearn.linear_model import LogisticRegression
	from sklearn.neighbors import KNeighborsClassifier
	from sklearn.naive_bayes import GaussianNB
	
	#setup the classifier
	classify =  MLPClassifier(hidden_layer_sizes = (100, 50), max_iter = 2000, activation = 'tanh', solver='lbfgs', random_state = 1)
	LR = LogisticRegression(random_state = 1, solver = 'lbfgs', multi_class='ovr')
	knn = KNeighborsClassifier()
	gnb = GaussianNB()
	

	# training the network
	classify.fit(train[0], train[1])
	LR.fit(train[0], train[1])
	knn.fit(train[0], train[1])
	gnb.fit(train[0], train[1])

	cn_pred = classify.predict(validate[0])
	lr_pred = LR.predict(validate[0])
	knn_pred = knn.predict(validate[0])
	gnb_pred = gnb.predict(validate[0])

	
	
	
	# assessing the accuracy of different models
	from sklearn.metrics import confusion_matrix
	# accuracy of LR
	cm = confusion_matrix(lr_pred, validate[1])
	
	print("Accuracy of LR = ", accuracy(cm))
	
	# accuracy of CNN
	cm = confusion_matrix(cn_pred, validate[1])
	print("Accuracy of CNN = ", accuracy(cm))
	
	
	# accruacy of knn
	cm = confusion_matrix(knn_pred, validate[1])
	print("Accuracy of KNN = ", accuracy(cm))
	
	# accruacy of gnb
	cm = confusion_matrix(gnb_pred, validate[1])
	print("Accuracy of GNB = ", accuracy(cm))
	
	save(classify)


	print("DONE")

def save(obj):

	pickle_out = open("cnn.pickle","wb")
	pickle.dump(obj, pickle_out)
	pickle_out.close()
def save_data(data):
	pickle_out = open("data.pickle","wb")
	pickle.dump(data, pickle_out)
	pickle_out.close()
def load_pm():
	return pickle.load(open("Prediction/cnn.pickle", "rb"))
def load_data():
	return pickle.load(open("Prediction/data.pickle", "rb"))
def convert_input(arr):
	arr = np.asarray(arr)
	
	new_df = pd.DataFrame({"day" : [arr[0]],
						   "time": [arr[1]],
						   "stop_num" : [int(arr[2])],
						   "rid": [float(arr[3])],
						   "start": [arr[4]],
						   "end": [arr[5]],
						   "init_delay": [int(arr[6])],
						   "end_delay": [get_delay_range(arr[7])]
						   }
						   )
	data = load_data()
	print(data)
	print("DATA DONE")
	data_s = data.append(new_df)
	
	
	encode = LabelEncoder()
	data_s['day'] = encode.fit_transform(data_s['day'])
	data_s['stop_num'] = encode.fit_transform(data_s['stop_num'])
	data_s['rid'] = encode.fit_transform(data_s['rid'])
	data_s['start'] = encode.fit_transform(data_s['start'])
	data_s['end'] = encode.fit_transform(data_s['end'])
	data_s['time'] = encode.fit_transform(data_s['time'])
	feature_scaler = StandardScaler()
	data_s[['day', 'stop_num', 'rid', 'start', 'end', 'time', 'init_delay']] = feature_scaler.fit_transform(data_s[['day', 'stop_num', 'rid', 'start', 'end', 'time', 'init_delay']])
	
	return data_s.iloc[:, 0:-1]


# 0 = no delay (on earlier)
# 1 = 1-3 min delay
# 2 = 4-5 min delay
# 3 = 6-10 min delay
# 4 = 11-14 min delay
#5 = 15-17 min delay
# 6 = 18-20 min delay
# 7 = 20+ min delay 

# get a classification response


def get_delay(dep_station, dest_station, init_delay, model):
	delay_range = {0 : "0 minutes", 1: "5 minutes ", 2: "10 minutes ", 3: "15 minutes ", 4: "20 minutes ",
					5: "25 minutes ", 6: "30 minutes ", 7: "30+ minutes "}
	station_stop = {"NRW" : "1", "DIS" : "2", "SMK" : "3", "IPS" : "4", "MNG" : "5",
					"COL" : "6", "SRA" : "7", "LST" : "7"}
	import datetime
	
	day = datetime.datetime.today().day
	time = datetime.datetime.now().strftime("%H")
	
	
	day_type = ''
	if day < 6:
		day_type = 'WEEKDAY'
	elif day == 6:
		day_type = 'SATURDAY'
	elif day == 7:
		day_type = 'SUNDAY'

	
	test = convert_input([day_type, get_time(int(time)), station_stop[dep_station], "202010296712173", dep_station, dest_station, init_delay, "0"]).values
				
	data_to_classify = test[len(test)-1]
	
	prediction = model.predict([data_to_classify])

	return delay_range[prediction[0]]














