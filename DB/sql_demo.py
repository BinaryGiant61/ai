import sqlite3
import csv
from DB import trainStation
connection = sqlite3.connect('trainline.db')

c = connection.cursor()

#c.execute("""CREATE TABLE trainStations(
#              name text,
#              abreviation text )""")
#

def read_in_stations_from_file():
    with open('stationsAbreviations.csv', encoding='utf-8-sig') as csvfile:
        stationReader = csv.reader(csvfile, delimiter=',')
        for row in stationReader:
            sql = ("INSERT INTO trainStations VALUES(?,?)",(row[0],row[1]))
            c.execute("INSERT INTO trainStations VALUES(?,?)",(row[0],row[1]))
def insert_station(trainStation):
    with connection:
        c.execute("INSERT INTO trainStations VALUES(:name,:abreviation)",{'name':trainStation.name,
                                                                      'abreviation':trainStation.abbreviated})
def get_station_abrev(name):

    c.execute('SELECT abreviation FROM trainStations where name= ?',(name,))
    return c.fetchall()

def get_station_by_name(name):

    c.execute('SELECT * FROM trainStations where name= ?',(name,))
    return c.fetchall()
def get_station_by_abrev_name(abreviation):

    c.execute('SELECT * FROM trainStations where abreviation= ?',(abreviation,))
    return c.fetchall()
def remove_station(name):
    with connection:
        c.execute("DELETE from employee WHERE name= ?",(name,))

#read_in_stations_from_file()
#connection.commit()
#
#c.execute("SELECT * FROM trainStations")
#result =c.fetchall()
#for each in result:
#    print(each)
result= get_station_by_abrev_name("NRW")
#print(get_station_abrev("Westgate-on-Se"))

#connection.commit()
#
#connection.close()
