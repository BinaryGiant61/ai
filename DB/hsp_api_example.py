import requests
import json

# Documentation can be found at
#   https://wiki.openraildata.com/index.php/HSP

# When registering at https://datafeeds.nationalrail.co.uk/
# you only need the HSP subscription
# The Real time Data feed is too much to deal with
# The On Demand Data Feeds might be useful
# 
# In 'Planned usage', mention you are using the HSP data 
# for educational purposes, for a project, and for a limited
# time
# The T & Cs should not be an issue, nor the limit on the
# number of requests an hour - but do be polite and do not
# swamp the web service with an excessive number of requests

api_url = "https://hsp-prod.rockshore.net/api/v1/serviceMetrics"
# api_url = "https://hsp-prod.rockshore.net/api/v1/serviceDetails"

headers = { "Content-Type": "application/json" }
auths = ("a.maltman61@gmail.com", "Hotline61!")

data = {
  "from_loc": "NRW",
  "to_loc": "LST",
  "from_time": "0700",
  "to_time": "0800",
  "from_date": "2016-07-01",
  "to_date": "2016-08-01",
  "days": "SATURDAY"
}

r = requests.post(api_url, headers=headers, auth=auths, json=data)
j= json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',',': '))
print(json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',',': ')))
