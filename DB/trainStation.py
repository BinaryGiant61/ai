class trainStation:

    def __init__(self,name,abbreviated):
        self.name=name
        self.abbreviated=abbreviated

    @property
    def __repr__(self):
        return "trainStation('{}','{}'".format(self.name,self.abbreviated)
