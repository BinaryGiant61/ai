from tkinter import *
from datetime import datetime
# from experta import *
import tkinter as tk
import textwrap
import time
# from dateutil.relativedelta import *
# from KE.realkKe import conversation
# from KE.utilMethods import getFactIndex
# from Webscraper.updated_scraping import webScrape, results
from KE.realkKe import conversation, Fact
from KE.utilMethods import getFactIndex
from NLP.nlp import extract_label_value
from NLP.nlp import init, load_model, spacy
from Prediction import PM2


def send_msg(event):
    # get last input entered by user
   send_on_click()


def send_on_click():
    # get last input entered by user
    get_user_msg = textentry.get("1.0", 'end-1c').strip()
    message = textwrap.fill(get_user_msg, 27)
    textentry.delete("0.0", END)
    time1 = current.strftime("%D - %H:%M \n")
    if message != "":
        chat_window.config(state=NORMAL)
        chat_window.config(foreground="black", font=("Arial", 12))
        chat_window.insert(END, message+"   "+ '\n\n', ("right"))
        chat_window.insert(END, time1, ("medium", "right", "color"))

        tags = extract_label_value(message,vocab, textcat, nlp)
        print("tags = ",tags)
        messageType = tags[0][0]
        extractedMessage = tags[0][1]
        if(messageType=='Date' or messageType=='Destination' or messageType=='Departure' or messageType=='Time'):
            message=extractedMessage
        print(message)
        facts1 = engine.facts.items()

        engine.modify(engine.facts[getFactIndex(engine.facts.items(), "responseTypesMatch")], checkedThisRun=False)
        engine.modify(engine.facts[getFactIndex(engine.facts.items(), "latest_response")], latest_response=message)
        engine.modify(engine.facts[getFactIndex(engine.facts.items(), "response_parsed")], response_parsed=False)
        engine.modify(engine.facts[getFactIndex(engine.facts.items(), "responseType")], responseType=messageType)
        facts = engine.facts.items()

        response_Type =engine.facts[getFactIndex(engine.facts.items(), "responseType")].get("responseType")

        facts = engine.facts.items()
        engine.run()

        chat_window.insert(END, time1, ("medium", "color"))
        chat_window.config(state=DISABLED)

        chat_window.yview(END)


def remove_place_holder(event):
    place_holder.place_forget()
    textentry.focus_set()


def add_place_holder(event):
    placeholder_flag = 0
    if placeholder_flag == 1:
        place_holder.place(x=6, y=422, height=68, width=264)

def onResize(event):
    global prev_width
    global prev_height
    if event.widget != window:
        return
    if event.width != prev_width or event.height != prev_height:
        prev_width = event.width
        prev_height = event.height

        # print(event)

        txt_width = (prev_width - (400 - 280))
        y_pos = (prev_height - (500 - 420))
        chat_height = (prev_height - (500 - 420)) - 13

        chat_window.place(width=(prev_width - (400 - 372)), height=chat_height)
        scroll.place(x=(prev_width - (400 - 374)), height=chat_height)

        textentry.place(y=y_pos, width=txt_width)
        place_holder.place(y=(y_pos - 2), width=txt_width)

        send_button.place(x=(txt_width + 10), y=y_pos)

def restartBooking():
    print("hello")
    engine.reset()
    print("muhuhauhua")
prev_width = 0
prev_height = 0

#### main
window = Tk()
window.title('National Rail Chatbot')
window.configure(background="darkgray")
window.geometry("400x500")
window.resizable(True, True)

current = datetime.now()

# chat window
chat_window = Text(window, bd=0, height="7.8", width="49", font="Arial", wrap="word")
chat_window.config(state=NORMAL)
chat_window.tag_config("right", justify="right")
chat_window.tag_config("medium", font=("Arial", 12))
chat_window.tag_config("color", foreground="darkgray")

chat_window.config(fg="black", font=("Arial", 12))
chat_window.config(state=DISABLED)

# adding scrollbar into the chat window
scroll = Scrollbar(window, command=chat_window.yview, cursor="arrow")
chat_window['yscroll'] = scroll.set

# creating send button
send_button = Button(window, font=("Arial", 12, 'bold'),
                     bd=0, text="Send", width="10", height=13, fg="black",
                     activebackground="black", bg="#383838", command=send_on_click)

# textbox for user to enter message
textentry = Text(window, bd=0, fg="black", bg="white", highlightcolor="white",
                 width="28", height="5", font=("Arial", 12), wrap="word")

# adding placeholder in textbox
place_holder = Text(window, bd=0, fg="lightgray", bg="white",
                    width="28", height="4", font=("Arial", 12), wrap="word")
place_holder.insert("1.0", "Enter message here")

# positioning components on the screen
chat_window.place(x=6, y=6, height=408, width=372)
scroll.place(x=374, y=6, height=408)
place_holder.place(x=6, y=418, height=68, width=274)
send_button.place(x=280, y=420, height=71)

textentry.place(x=6, y=420, height=71, width=274)

place_holder.bind("<FocusIn>", remove_place_holder)
textentry.bind("<FocusOut>", add_place_holder)

window.bind('<Return>', send_msg)
print("loading engine")
engine = conversation()
engine.reset()
engine.textwindow=chat_window
engine.model=PM2.load_pm()
print("engine loaded")
vocab =init()
print("vocab loaded")
textcat = load_model()
print("model loaded")
nlp = spacy.load("en_core_web_sm")
window.bind('<Configure>', onResize)

window.bFlag = 0

window.mainloop()
