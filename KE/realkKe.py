import sqlite3
import textwrap
from functools import partial
from tkinter import *
import webbrowser
from experta import *
from experta.factlist import FactList
from datetime import datetime

from experta.utils import frozenlist

from DB.sql_demo import get_station_by_name, get_station_by_abrev_name, get_station_abrev
from KE.HyperLinkManager import HyperlinkManager
from KE.utilMethods import roundMins, getFactIndex, dateInBounds
from Prediction.PM2 import get_delay
from Webscraper.updated_scraping import webScrapSingle, webScrapeReturn


from dateutil.relativedelta import *

class response(Fact):
    pass

class conversation(KnowledgeEngine):
    textwindow=None
    model = None
    connection = sqlite3.connect('trainline.db')
    c = connection.cursor()
    @DefFacts()
    def _initial_action(self,path="",opener=False):
        yield response(data={
            "expectedType": "Date",
            "actualType": "time"

        })
        yield Fact(responseTypesMatch=True,checkedThisRun=False)
        yield Fact(response_parsed=False)
        yield Fact(responseType="Any")
        yield Fact(responseType="")
        yield Fact(latest_response="")
        yield Fact(path=path)
        if opener:
            yield Fact(opener=True)
            yield Fact(expected_response=["Date"])
            self.restartedBooking()
        else:
            yield Fact(expected_response=["Any"])




    @Rule(AND(
              Fact(responseType=MATCH.responseType),
              Fact(expected_response=MATCH.expected_response),
              Fact(responseTypesMatch=W(),checkedThisRun=False),
              Fact(response_parsed=False)
          ))
    def checkResponseTypes(self,responseType,expected_response):
        if("Any" in expected_response):
            self.modify(self.facts[getFactIndex(self.facts.items(), "responseTypesMatch")], responseTypesMatch=True,checkedThisRun=True)
        elif(responseType in expected_response):
            self.modify(self.facts[getFactIndex(self.facts.items(), "responseTypesMatch")],
                          responseTypesMatch=True,checkedThisRun=True)
        else:
            self.modify(self.facts[getFactIndex(self.facts.items(), "responseTypesMatch")], responseTypesMatch=False,checkedThisRun=True)

    @Rule(AND(
        Fact(latest_response=MATCH.latest_response),
        Fact(response_parsed=False),
        Fact(path=""),
        NOT(Fact(opener=W()))
    ))
    def askForPath(self):
        self.textwindow.insert(END, textwrap.fill("Hi would you like to purchase a train tickets today "
                                   "or predict delays for a train ?", 30) + '\n\n')
        self.declare(Fact(opener=True))
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)


    @Rule(AND(
              Fact(latest_response=MATCH.latest_response),
              Fact(response_parsed=False),
              NOT(OR((Fact(path='tickets'))),(Fact(path='delay')))))

    def decidePath(self,latest_response):

        latest_response=str.lower(latest_response)
        if (not("tickets" in latest_response) and not("delays" in latest_response )):
            self.textwindow.insert(END, textwrap.fill("I am sorry I don't understand  %s please choose "
                                        "either tickets or delays" % latest_response, 30) + '\n\n')
        if("ticket" in latest_response):
            self.declare(Fact(path="tickets"))
            self.textwindow.insert(END, textwrap.fill("When would you like a ticket for ?", 30) + '\n\n')
            self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Date"])
        elif ("delay" in latest_response):
            self.declare(Fact(path="delay"))
            self.textwindow.insert(END, textwrap.fill("What was the last station you departed from ?", 30) + '\n\n')
            self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Departure,Junk"])
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)


    def restartedBooking(self):
        self.textwindow.insert(END, textwrap.fill("When would you like a ticket for ?", 30) + '\n\n')

    @Rule(AND(
         (Fact(path= "tickets")),
        (Fact(responseTypesMatch=True)),
        Fact(response_parsed=False),

        NOT(Fact(depDate=W()))))
    def askDeparture(self):
        depDate =self.facts[getFactIndex(self.facts.items(), "latest_response")].get("latest_response")


        inBounds= dateInBounds(self,datetime.now().date(),depDate)
        if(inBounds):
            self.declare(Fact(depDate=depDate))
            self.textwindow.insert(END, textwrap.fill("When on %s do you want to depart by ?"%depDate, 30) + '\n\n')
            self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Time"])

        else:
            self.textwindow.insert(END, textwrap.fill("The date entered is out of range ,please enter a day within "
                                                      "the next 3 months", 30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)


    @Rule(AND((Fact(path= "tickets")),
               Fact(latest_response=MATCH.latest_response),
               (Fact(responseTypesMatch=True)),
               Fact(response_parsed=False),
               Fact(depDate=MATCH.depDate),
               NOT(Fact(depTime=W()))))
    def askDepartTimes(self,latest_response,depDate):

        depTime = roundMins("up",latest_response,15)
        present = datetime.now().time()
        presentDate =datetime.today().date()
        if(depDate==presentDate):
            if depTime<=present:
                self.textwindow.insert(END, textwrap.fill("The time entered is out of range ,please enter a valid time set in the future", 30) + '\n\n')
            else:
                self.declare(Fact(depTime=depTime))
        else:
            self.declare(Fact(depTime=depTime))
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)


    @Rule(AND(
              Fact(depTime=W()),
              NOT(Fact(startLocation=W(),abbreviation=W()))
              ))
    def askStartLocation(self):
        self.textwindow.insert(END, textwrap.fill("What station do you want to depart from?" , 30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Departure","Junk"])

    @Rule(AND(
              Fact(latest_response=MATCH.latest_response),
              Fact(responseType=MATCH.responseType),
              (Fact(responseTypesMatch=True)),
              Fact(response_parsed=False),
                    OR(AND(Fact(path="tickets"),Fact(depTime=W())),(Fact(path="delay"))),

              NOT(Fact(startLocation=W(),abbreviation=W()))
          ))
    def startLocation(self,latest_response,responseType):
        station=[]
        if(responseType=='Departure'):
            station = get_station_by_abrev_name(latest_response)

        elif(responseType=="Junk"):
            station = get_station_by_name(latest_response)

        if (len(station) == 1):
            self.declare((Fact(startLocation=station[0][0], abbreviation=station[0][1])))
        else:
            self.textwindow.insert(END, textwrap.fill("I'm sorry I don't recognise that station please re-enter your station", 30) + '\n\n')


        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)


    @Rule(AND(
              Fact(startLocation=W(),abbreviation=W()),
              NOT(Fact(endLocation=W(),abbreviation=W()))
              ))
    def askEndLocation(self):
        self.textwindow.insert(END,textwrap.fill("What station is your destination ?",30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Destination","Junk"])

    @Rule(AND(
        Fact(latest_response=MATCH.latest_response),
        Fact(responseType=MATCH.responseType),
        Fact(responseTypesMatch=True),
        Fact(response_parsed=False),
        Fact(startLocation=W(), abbreviation=W()),
        OR(Fact(path="tickets"), (Fact(path="delay"))),
        NOT( Fact(endLocation=W(),abbreviation=W()))
    ))
    def endLocation(self,latest_response,responseType):
        station = []
        if (responseType == "Destination"):
            station = get_station_by_abrev_name(latest_response)

        elif (responseType == "Junk"):
            station = get_station_by_name(latest_response)

        if(len(station)==1):
            self.declare((Fact(endLocation=station[0][0],abbreviation=station[0][1])))

        else:
            self.textwindow.insert(END, textwrap.fill("I'm sorry I don't recognise that station please re-enter your "
                                                      "station",30) + '\n\n')

        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

    @Rule(AND(Fact(path="delay"),
              Fact(startLocation=MATCH.startLocation, abbreviation=W()),
              Fact(endLocation=W(), abbreviation=W())
              ))
    def requestTimeFromDep(self,startLocation):
       self.textwindow.insert(END, textwrap.fill("How many minutes were you delayed at %s ?"%startLocation,
                                                 30) + '\n\n')
       self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Time","Junk"])

    @Rule(AND(Fact(path="delay"),
              Fact(latest_response=MATCH.latest_response),
              (Fact(responseTypesMatch=True)),
              Fact(responseType=MATCH.responseType),
              Fact(response_parsed=False),
              Fact(endLocation=W(), abbreviation=W()),
              NOT(Fact(departDelay=W()))
              ))
    def getTimeFromDep(self, latest_response,responseType):
       print("hi")
       if(responseType=="Junk"):
            isitInt =latest_response.isnumeric()
            if(isitInt):

                self.declare(Fact(departDelay = latest_response))
                self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)
            else:
                self.textwindow.insert(END,
                                       textwrap.fill("I'm sorry that isn't a valid number of minutes, please try again", 30) + '\n\n')
       else:
           self.declare(Fact(departDelay=latest_response))

       self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

    @Rule(AND(Fact(departDelay =MATCH.departDelay),
              Fact(startLocation=MATCH.startLocation, abbreviation=W()),
              Fact(endLocation=MATCH.endLocation, abbreviation=W()),
              NOT(Fact(bookingFinished=True))
              ))
    def returnExpectedDelay(self,departDelay,startLocation,endLocation):
        start = self.facts[getFactIndex(self.facts.items(), "startLocation")]
        end  =self.facts[getFactIndex(self.facts.items(), "endLocation")]
        delayTime =get_delay(start.get('abbreviation'), end.get('abbreviation'), departDelay, self.model)
        self.textwindow.insert(END, textwrap.fill("you should be  delayed aproximately %s "%(delayTime) ,
                                                  30) + '\n\n')
        self.declare(Fact(bookingFinished=True))



    @Rule(AND(Fact(path="tickets"),
              Fact(startLocation=MATCH.startLocation,abbreviation=W()),
              Fact(endLocation=MATCH.endLocation,abbreviation=W()),
              Fact(depDate=MATCH.depDate),
              Fact(depTime=MATCH.depTime),
              NOT(Fact(outwardConfirmed=W()))
              )
          )
    def checkTicketDetails(self,startLocation,endLocation,depDate,depTime):
        self.textwindow.insert(END, textwrap.fill("so you want to catch a train from %s to %s on %s at %s"
                                           %(startLocation,endLocation,depDate,depTime), 30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Confirmation","Rejection"])


    @Rule(AND(Fact(path="tickets"),
              Fact(responseType=MATCH.responseType),
              Fact(response_parsed=False),
              Fact(endLocation=W(), abbreviation=W()),
              Fact(responseTypesMatch=True),
             NOT( Fact(outwardConfirmed=W())),

              )
            )
    def ticketConfirmation(self,responseType):
        self.declare(Fact( outwardConfirmed=responseType))
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

# negative confirmation flow
    @Rule(AND(Fact(outwardConfirmed="Rejection")))
    def askAboutRestart(self):
        self.textwindow.insert(END, textwrap.fill("Would you like to start a fresh booking ?",
                                                  30) + '\n\n')

    @Rule(AND(
        Fact(responseType=MATCH.responseType),
        Fact(responseTypesMatch=True),
        Fact(response_parsed=False),
        Fact(outwardConfirmed="Rejection"),
        NOT(Fact(restart=W()))
    ))
    def determineRestartOrEnd(self,responseType):
        self.declare((Fact(restart=responseType)))
        if(responseType=="Confirmation"):
            self.reset(path="tickets", opener=True)
        elif (responseType=="Rejection"):
            self.declare(Fact(bookingFinished=True))

    ##postive confirmation flow
    @Rule(AND(Fact(outwardConfirmed="Confirmation")))
    def askAboutReturn(self):
            self.textwindow.insert(END, textwrap.fill("Would you like to make this a return journey?",
                                                      30) + '\n\n')



    @Rule(AND(
             Fact(responseType=MATCH.responseType),
             Fact(responseTypesMatch=True),
             Fact(response_parsed=False),
             Fact(outwardConfirmed="Confirmation"),
             NOT(Fact(returnJourney=W()))
             ))
    def askReturnDate(self, responseType):

        self.declare((Fact(returnJourney=responseType)))
        if(responseType=="Confirmation"):
            self.textwindow.insert(END, textwrap.fill("When would you like to return ?",30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Date"])

    @Rule(AND(Fact(latest_response=MATCH.latest_response),
              Fact(responseTypesMatch=True),
              Fact(response_parsed=False),
              Fact(depDate=MATCH.depDate),
              NOT(Fact(retDate=W())),
              (Fact(returnJourney="Confirmation"))
              ))
    def returnDate(self, latest_response, depDate):
        retDate = latest_response
        inBounds = dateInBounds(self, depDate, retDate)
        if (inBounds):
            self.declare(Fact(retDate=retDate))
        else:
            self.textwindow.insert(END, textwrap.fill("The date entered is out of range ,please enter a day within the next 3 months"
                                                      "and after outward bound train",
                                                      30) + '\n\n')



    @Rule(AND(Fact(retDate=MATCH.retDate),
              NOT(Fact(retTime=W()))))
    def askRetTime(self, retDate):
        self.textwindow.insert(END, textwrap.fill("When on %s do you want to return?"%retDate,30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Time"])

    @Rule(AND(Fact(latest_response=MATCH.latest_response),
              Fact(responseTypesMatch=True),
              Fact(response_parsed=False),
              Fact(retDate=MATCH.retDate),
              Fact(depTime=MATCH.depTime),
              Fact(depDate=MATCH.depDate),
              NOT(Fact(retTime=W()))
              ))
    def retTime(self, latest_response, retDate, depTime, depDate):
        retTime = roundMins("up",latest_response,15)
        if (retDate == depDate):
            if retTime <= depTime:
                self.textwindow.insert(END, textwrap.fill(
                    "The time entered is out of range,please enter a valid time set after your outwardbound train departs",
                    30) + '\n\n')

            else:
                self.declare(Fact(retTime=retTime))
        else:
            self.declare(Fact(retTime=retTime))
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

    @Rule(AND(Fact(retDate=MATCH.retDate),
              Fact(retTime=MATCH.retTime),
              NOT(Fact(confirmedReturn=W()))))
    def returnDetails(self,retDate,retTime):
        self.textwindow.insert(END, textwrap.fill("So you want to return on %s at %s?"%(retDate,retTime),30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")], expected_response=["Confirmation","Rejection"])


    @Rule(AND(
              Fact(responseTypesMatch=True),
              Fact(response_parsed=False),
              Fact(retTime=W()),
              Fact(responseType=MATCH.responseType),
              NOT(Fact(confirmedReturn=W()))
              )
          )
    def confirmReturnDetails(self,responseType):
        self.facts.declare(Fact(confirmedReturn=responseType))
        if(responseType=="Rejection"):
            self.textwindow.insert(END, textwrap.fill(
            "Would you like to restart your journey booking? If not your journey will be cancelled",
            30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)




    @Rule(AND(Fact(confirmedReturn="Rejection"),
              Fact(responseTypesMatch=True),
              Fact(response_parsed=False),
              Fact(responseType=MATCH.responseType)
              )

    )
    def restartReturn(self,responseType):
        if(responseType=="Confirmation"):
            self.reset(path="tickets", opener=True)
        else:
            self.declare(Fact(bookingFinished=True))

    @Rule(Fact(outwardConfirmed="Confirmation"),
          Fact(startLocation=W(),abbreviation=MATCH.startAbrev),
          Fact(endLocation=W(),abbreviation=MATCH.endAbrev),
          Fact(depDate=MATCH.depDate),
          Fact(depTime=MATCH.depTime),
          (Fact(returnJourney="Rejection"))
          )
    def getOneWayTicket(self,startAbrev,endAbrev,depDate,depTime):
        depDate=depDate.strftime("%d%m%Y")
        depTime=depTime.strftime("%H%M")
        results =webScrapSingle(startAbrev,endAbrev,depDate,depTime)
        hyperlink = HyperlinkManager(self.textwindow)
        self.textwindow.insert(END, textwrap.fill("The cheapest ticket matching your criteria is  £ %s"%results[2],30) + '\n\n')
        self.textwindow.insert(END, textwrap.fill("Your train departs from %s at %s and arrives at %s at %s"%(startAbrev,results[0],endAbrev,results[1]) + '\n\n'))
        self.textwindow.insert(INSERT, textwrap.fill(" here is your ticket ", 30) + '\n\n',
                               hyperlink.add(partial(webbrowser.open, results[3])))
        self.declare(Fact(bookingFinished=True))


    @Rule((Fact(outwardConfirmed="Confirmation")),
          Fact(startLocation=W(), abbreviation=MATCH.startAbrev),
          Fact(endLocation=W(), abbreviation=MATCH.endAbrev),
          Fact(depDate=MATCH.depDate),
          Fact(depTime=MATCH.depTime),
          Fact(retDate=MATCH.retDate),
          Fact(retTime=MATCH.retTime),
          Fact(confirmedReturn="Confirmation"))
    def getReturnTicket(self,startAbrev,endAbrev,depDate,depTime,retDate,retTime):
        depDate = depDate.strftime("%d%m%Y")
        depTime = depTime.strftime("%H%M")
        retDate = retDate.strftime("%d%m%Y")
        retTime = retTime.strftime("%H%M")
        results = webScrapeReturn(startAbrev,endAbrev,depDate,depTime,retDate,retTime)
        hyperlink = HyperlinkManager(self.textwindow)
        self.textwindow.insert(END, textwrap.fill("The cheapest  RETURN ticket matching your criteria is £ %s" % results[4],30) + '\n\n')
        self.textwindow.insert(END, textwrap.fill("Your outward bound train departs from %s at %s and arrives at %s at %s"
                                                  % (startAbrev, results[0], endAbrev, results[1]),30) + '\n\n')
        self.textwindow.insert(END, textwrap.fill("Your return train departs from %s at %s and arrives at %s at %s" %
                                                  (endAbrev, results[2], startAbrev, results[3]), 30) + '\n\n')
        self.textwindow.insert(INSERT, textwrap.fill(" here is your ticket ",30)+'\n\n',
                               hyperlink.add(partial(webbrowser.open, results[5])))
        self.declare(Fact(bookingFinished=True))

    @Rule((Fact(bookingFinished=True)))
    def startAnew(self):
        self.textwindow.insert(END, textwrap.fill("Thank you for using the national rail chat bot , enter anything to "
                                                  "begin a new conversation" ,30) + '\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "expected_response")],
                    expected_response=["Any"])
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

    @Rule(AND(Fact(bookingFinished=True),
            Fact(responseTypesMatch=True),
            Fact(response_parsed=False)))
    def resetCanvas(self):
        self.textwindow.delete('1.0', END)
        self.reset()

    @Rule(AND(
              Fact(expected_response=MATCH.expected_response),
              Fact(responseTypesMatch=False),
              Fact(response_parsed=False)))
    def askForValidInput(self ,expected_response):
        if(isinstance(expected_response,frozenlist)):
            expected_response=expected_response[0]
        self.textwindow.insert(END,textwrap.fill("I am sorry I don't understand that response ,I am looking for you to respond with a %s" % (expected_response) )+'\n\n')
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)
