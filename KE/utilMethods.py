from datetime import datetime, timedelta
from tkinter import END

from dateutil.relativedelta import relativedelta
from experta import Fact


def getFactIndex(FactList,factName):
    result = None

    for d in FactList:
        if(d[1].get(factName)!=None):
            result=d[0]
            break

    return result

def roundMins(direction, time,res):
    remainder = time.minute%15
    delta = timedelta(minutes =15-remainder)
    if(time.minute%15!=0):
        date= datetime(2017, 11, 28, time.hour,time.minute,0,0)
        time=(date+(delta)).time()

    return time

def dateInBounds(self, minDate,selected_date):
    selected_date = selected_date
    present = datetime.now().date()
    cutoff = present + relativedelta(days=+83)
    inBounds=True
    if (selected_date < minDate or selected_date > cutoff):
        inBounds= False
    self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)
    return inBounds

def checkTime(self, latest_response, retDate, depTime, depDate):
        retTime = roundMins("up", datetime.strptime(latest_response, "%H:%M"), 15)
        if (retDate == depDate):
            if retTime <= depTime:
                self.textwindow.insert(END, "\nBot :The time entered is out of range ,"
                                            "please enter a valid time set after your outwardbound train departs")

            else:
                self.declare(Fact(returnTime=retTime))
        else:
            self.declare(Fact(returnTime=retTime))
        self.modify(self.facts[getFactIndex(self.facts.items(), "response_parsed")], response_parsed=True)

#now = datetime.now()
#depTime = datetime.strptime(input("When on %s do you want to depart by ?" ), "%H:%M")
#present = datetime.now().time()
#depTime = roundMins("up",depTime,15)
#rounded = roundMins("up",datetime.now() , 15)
#print(depTime)