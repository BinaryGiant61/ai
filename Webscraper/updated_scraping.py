import csv

import requests
from bs4 import BeautifulSoup
import os
from datetime import datetime
import time
#from dateutil.relativedelta import relativedelta
#from dateutil import parser
import csv, re
#import pandas as pd



def webScrapSingle(startLocation,endLocation,depDate,depTime):
    url = ("https://ojp.nationalrail.co.uk/service/timesandfares/%s/%s/%s/%s/dep"%(startLocation,endLocation,depDate,depTime))
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    # departure time
    ticket = soup.find("div", {"class": "cheapest"}).find_parent('tr')

    ticket_from = soup.find("td", {"class": "results-from"})
    dept_time = ticket.find("div", {"class": "dep"}).get_text().strip()


    # arrival time
    arrival_time =ticket.find("div", {"class": "arr"}).get_text().strip()

    # price of cheapest ticket
    cheapest_price = soup.find("button", {"id": "buyCheapestButton"}).get_text()
    cheapest_price = cheapest_price.strip()
    cheapest_price = cheapest_price.split('£')[1]


    return [dept_time,arrival_time,cheapest_price,url]

def webScrapeReturn(startLocation,endLocation,depDate,depTime,retDate,retTime):

    url = ("https://ojp.nationalrail.co.uk/service/timesandfares/%s/%s/%s/%s/dep/%s/%s/dep"%(startLocation,endLocation,depDate,depTime,retDate,retTime))

    r = requests.get(url)

    soup = BeautifulSoup(r.text, 'html.parser')
    #dept times


    tickets = soup.findAll("div", {"class": "cheapest"})

    returnTicket= tickets[1].find_parent('tr')
    outwardTicket = tickets[0].find_parent('tr')
    dept_time = outwardTicket.find("div", {"class": "dep"}).get_text().strip()
    return_dep_time= returnTicket.find("div", {"class": "dep"}).get_text().strip()

    # arrival time

    arrival_time = outwardTicket.find("div", {"class": "arr"}).get_text().strip()
    return_arrival_time = returnTicket.find("div", {"class": "arr"}).get_text().strip()


    # price of cheapest ticket
    cheapest_price = soup.find("button", {"id": "buyCheapestButton"}).get_text()
    cheapest_price=cheapest_price.strip()
    cheapest_price=cheapest_price.split('£')[1]


    return [dept_time,arrival_time,return_dep_time,return_arrival_time,cheapest_price,url]

#details =webScrapeReturn('NRW','LBG','10022021','0930','10022021','1800')
#details =webScrapSingle('NRW','LST','190221','0930')

print("adadad")