import spacy
import pickle
from spacy.matcher import PhraseMatcher
import NLP.KB as KB


'''
what to do:
- make it work for specific dates (28/2/2021) Rule based entity detection 
- make it work for verbal dates (5th of feb) 
- recognise locations  (build a KB for this)
- suggest locations
- get rid of introduction 
- detect times Rule based entity detection 
- format times properly
- format stations
- update model method
- response builder
'''


vocab = None


def init():
	nlp = spacy.load("en_core_web_sm")
	from spacy.vocab import Vocab
	vocab = Vocab(strings=KB.get_stations())
	
	return vocab

# load in data for model

def read_training_file():
	training_data = []

	import csv
	with open('data.csv', newline='') as csvfile:
		training = csv.reader(csvfile, delimiter = ' ', quotechar='|')
		for row in training:
			labels = row[0].split(",")
			training_data.append((labels[0], labels[1]))

	
	return training_data


# format training data
# [ (value), dict ( label : True, label: False), ... ]

def format_training_data(data):

	values = []
	labels = []

	# extract values and labels from list
	for item in data:
		values.append(item[1])
		labels.append({'cats': {'Introduction' : item[0] == 'Introduction',
								'Closing'      : item[0] == 'Closing',
								'Destination'      : item[0] == 'Destination',
								'Departure'        : item[0] == 'Departure',
								'Date'             : item[0] ==  'Date',
								'Time'             : item[0] == 'Time',
								'Confirmation'     : item[0] == 'Confirmation',
								'Rejection'        : item[0] ==  'Rejection',
								'ReturnTicket'     : item[0] == 'ReturnTicket',
								'Ticket'           : item[0] == 'Ticket',
								'TrainDelay'       : item[0] == 'TrainDelay'
							   }})


	data_to_train = list(zip(values, labels))

	return data_to_train
	
	
# train data
def train(nlp, data):


	from spacy.util import minibatch
	import random
	random.seed(1)
	spacy.util.fix_random_seed(1)
	optimizer = nlp.begin_training()

	losses = {}

	# training loop
	for epoch in range(10000):
		random.shuffle(data)
		batches = minibatch(data, size = 8)
		
		for batch in batches:
			values, labels = zip(*batch)
			nlp.update(values, labels, sgd = optimizer, losses = losses)


	cat = nlp.get_pipe('textcat')

	
	# save the model
	cat.to_disk("NLP")


# create text categorizer
''' 
	creates a text categorizer and saves it locally (to be loaded to classify)
'''
def create_cat(data):
	nlp = spacy.blank("en")

	# setup bag of words format for categorizing
	cat = nlp.create_pipe("textcat", config = {"exclusive_classes": True, "architecture": "bow"})

	nlp.add_pipe(cat)

	# add classification labels
	cat.add_label("Introduction")
	cat.add_label("Closing")
	cat.add_label("Destination")
	cat.add_label("Departure")
	cat.add_label("Date")
	cat.add_label("Time")
	cat.add_label("Confirmation")
	cat.add_label("Rejection")
	cat.add_label("ReturnTicket")
	cat.add_label("Ticket")
	cat.add_label("TrainDelay")
	
	train(nlp, data)
	
	return cat

# load model

def load_model():
	from spacy.pipeline import TextCategorizer
	nlp = spacy.load("en_core_web_sm")
	
	textcat = TextCategorizer(nlp.vocab)
	

	textcat.from_disk("NLP")

	return textcat


# extract data
'''
	This will extract the relevant data from the given text input.
	For example, if the input is "I want to go to Norwich", Norwich will be
	extracted and labelled as "data". 
'''
def extract_station(str):

	# text preprocess

	nlp = spacy.load("en_core_web_sm")
	
	doc = nlp(str)
	
	
	matcher = PhraseMatcher(nlp.vocab, attr='LOWER')
	terms = ['Norwich', 'London']
	
	patterns = [nlp(text) for text in terms]
	matcher.add("list", patterns)
	

	
	match = matcher(doc)
	index_first = match[0][1]
	index_last = match[0][2]
	
	
	
	
	return doc[index_first:index_last]
	
# extract label
'''
	This will extract the intended label which describes the type of input.
	For example, if it is "I want to go to Norwich", the label will be "Destination" type
	as you have go to within the sentence.
'''
def calculate_date(week_str):

	

	import datetime
	from datetime import date
	
	current_day_multiplier = 0
	today_date = date.today()
	first_date_of_next_month = (today_date.replace(day=1) + datetime.timedelta(days=32)).replace(day=1)
	date_diff = first_date_of_next_month - today_date
	
	days_until_next_month = date_diff.days

	
	
	nlp = spacy.load("en_core_web_sm")
	doc = nlp(week_str)
	
	has_month = False
	has_week = False
	
	
	list_of_tokens = [token.lemma_ for token in doc]
	print(list_of_tokens)
	month_count = list_of_tokens.count("month")
	week_count = list_of_tokens.count("week")
	
	if month_count > 0:
		has_month = True
		current_day_multiplier = 1
	if week_count > 0:
		has_week = True
		current_day_multiplier = 1
		current_day_multiplier = current_day_multiplier * 7
	
	for item in list_of_tokens:
		if item.isnumeric() == True and has_month == False:
			current_day_multiplier = current_day_multiplier * int(item)
		if item.isnumeric() == True and has_month == True:
			first_date_of_the_month = (today_date.replace(day=1) + datetime.timedelta(days = int(item) *32)).replace(day=1)
			date_diff = first_date_of_the_month - today_date
			current_day_multiplier = current_day_multiplier + date_diff.days
			
	dt_o = None
	if current_day_multiplier > 0:
		calculated_date = today_date + datetime.timedelta(current_day_multiplier)
		dt_o = datetime.datetime.combine(calculated_date, datetime.datetime.min.time())
		dt_o=dt_o.date()	


	return dt_o
	
# format consistently 
def format(str):
	

	return ""

# ensure station is valid
def check_station(str):
	# { "StationName" : CODE, ... }
	# { "StationName" : LOCATION } 
	# https://www.ukpostcode.co.uk/IP11-district.htm
	# https://en.wikipedia.org/wiki/UK_railway_stations_–_A 
	# https://geoportal.statistics.gov.uk/datasets/4f71f3e9806d4ff895996f832eb7aacf
	
	
	return None
def get_day(str, nlp):
	from spacy.matcher import PhraseMatcher
	matcher = PhraseMatcher(nlp.vocab, attr='LOWER')

	days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Today', 'Tomorrow']
	patterns = [nlp(day) for day in days]
	matcher.add("Days", patterns)

	doc = nlp(str)
	matches = matcher(doc)
	the_day = None
	new_date = None
	
	if (len(matches) > 0):
		for match_id, start, end in matches:
			string_id = nlp.vocab.strings[match_id]  # Get string representation
			span = doc[start:end]  # The matched span
			the_day = span.text

		print(the_day.lower())


		day_dict = {"monday": 0, "tuesday": 1, "wednesday": 2, "thursday": 3, "friday": 4, "saturday": 5, "sunday": 6, "today": -1, "tomorrow": -2}
		day_num = day_dict[the_day.lower()]
		print(day_dict[the_day.lower()])

		import datetime

		if(day_num == -1):
			new_date = datetime.datetime.today()
		elif(day_num == -2):
			new_date = datetime.datetime.today() + datetime.timedelta(1)
		else:
			diff = (day_num - datetime.datetime.today().weekday()) % 7

			if(diff == 0):
				diff = 7
			
			new_date = datetime.datetime.today() + datetime.timedelta(diff)
		
	return new_date
def time_date_matcher(str, nlp):
	import datetime
	#index_of_space = str.index(' ')
	#print('index: ',index_of_space)
	#str = str.replace(' ', '')
	
	try:
		# determine if date pattern detected
		from spacy.matcher import Matcher
		matcher = Matcher(nlp.vocab)
		pattern = [{"TEXT": {"REGEX": "(^(((0?[1-9]|1[0-9]|2[0-8])[\/](0?[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)"}}]
		text_date_pattern = [{"TEXT": {"REGEX": "(\d+(nd|st|th|rd)|(January|February|March|April|May|June|July|August|September|October|November|December))"}}]
		time_pattern = [{"TEXT": {"REGEX": "([0-1][0-2]:[0-5][0-9](pm|am))|([0-9]:[0-5][0-9](pm|am))|([0-2][0-3]:[0-5][0-9])|([0-1][0-9]:[0-5][0-9])|([0-1][0-2](pm|am))|([0-9](pm|am))|([0-9]:[0-5][0-9])"}}]
		
		
		#simple time pattern
		# regex date source: https://stackoverflow.com/questions/15491894/regex-to-validate-date-format-dd-mm-yyyy
		matcher.add("date", None, pattern)
		matcher.add("textdate", None, text_date_pattern)
		matcher.add("time", None, time_pattern)

		
		doc = nlp(str)
		matches = matcher(doc)
		print(matches)
		
		if (len(matches) == 0):
			return
		
		results = []

		for match_id, start, end in matches:
			string_id = nlp.vocab.strings[match_id]  # Get string representation
			span = doc[start:end]  # The matched span
			print(match_id, string_id, start, end, span.text)
			results.append((string_id, span.text))

		
		extracted_data = []
		
		text_date_data = [result[1].lower() for result in results if result[0] == 'textdate']
		date_data = [result[1].lower() for result in results if result[0] == 'date']
		time_data = [result[1].lower() for result in results if result[0] == 'time']
		
		if len(text_date_data) > 0:
			# month recogniser
			nlp = spacy.load("en_core_web_sm")
			from spacy.matcher import PhraseMatcher
			matcher = PhraseMatcher(nlp.vocab, attr='LOWER')	
			months = KB.get_year_dict()
			patterns = [nlp(month) for month in months]
			matcher.add("Months", patterns)
			
			date_str = " ".join(text_date_data)
			doc = nlp(date_str)
			matches = matcher(doc)
			
			month = None
			for match_id, start, end in matches:
				string_id = nlp.vocab.strings[match_id]  # Get string representation
				span = doc[start:end]  # The matched span
				print(match_id, string_id, start, end, span.text)
				month = span.text
			
			print(month)
			
			num_str = ''
			for char in date_str:
				if(char.isdigit() == True):
					num_str = num_str + char
				
			day_of_month = int(num_str)
			
			print(day_of_month)
			
			month_str = KB.dict_year_num(month.capitalize())
			print(month_str)
			
			the_date = datetime.date(datetime.datetime.now().year, int(month_str), int(day_of_month))
			extracted_data.append(('Date', the_date))
		if len(date_data) > 0:
			date_str = date_data[0].split("/")
			print(date_str)
			the_date = datetime.date(int(date_str[2]), int(date_str[1]), int(date_str[0]))
			
			extracted_data.append(('Date', the_date))
		if len(time_data) > 0:
			print(time_data[0])
			
			# count number of ms to determine if it contains am/pm
			m_count = time_data[0].count('m')
			
			if m_count == 0:
				time_str = time_data[0].split(":")
				the_time = datetime.time(int(time_str[0]), int(time_str[1]))
				
				extracted_data.append(('Time', the_time))
			else:
				clock_split = time_data[0].split("m")[0][-1] + 'm'

				if clock_split == 'pm':
					time_str = time_data[0].split("pm")[0].split(":")
					the_time = datetime.time(int(time_str[0])+12, int(time_str[1]))
					
					extracted_data.append(('Time', the_time))
				elif clock_split == 'am':
					time_str = time_data[0].split("am")[0].split(":")
					the_time = datetime.time(int(time_str[0]), int(time_str[1]))
					
					extracted_data.append(('Time', the_time))
	except:
		print("Error occurred")
		return None
		
	
	return extracted_data
	
	
	
def build_loc_matcher():
	nlp = spacy.load("en_core_web_sm")
	from spacy.matcher import PhraseMatcher
	matcher = PhraseMatcher(nlp.vocab, attr='LOWER')
	
	stations = KB.get_stations()
	patterns = [nlp(station) for station in stations]
	matcher.add("Stations", patterns)
	
	
	pickle.dumps(matcher)
	

def location_matcher(str):

	matcher = pickle.load( open( "locations.pk1", "rb" ) )
	
	print(matcher)
	
	
	
	
def extract_label_value(str, vocab, textcat, nlp):


	# junk [<0.2] possible_junk [0.21-0.4] something [0.41+]

	labels = []
	
	#tokenize input string
	doc = nlp(str)
	docs = [nlp.tokenizer(str)]
	

	label_detected = False
	#print(str)
	
	
	org = None
	

	# check time and date 
	date_match = time_date_matcher(str, nlp)
	
	if date_match != None:
		for dates in date_match:
			labels.append(dates)
	else:
		day = get_day(str, nlp)
		if(day != None):
			labels.append(('Date', day.date()))
	for ent in doc.ents:
		print(ent.label_)
		if ent.label_ == 'DATE':
			date = calculate_date(ent.text)
			if(date != None):
				labels.append(('Date', date))

	
	# load in categorizer
	
	scores, _ = textcat.predict(docs)
	prediction = scores.argmax(axis = 1)
	
	scores_list = scores.tolist()[0]
	#print(scores_list)
	

	
	prediction = ([textcat.labels[label] for label in prediction])

	value = True
	label = prediction[0]
	
	if prediction[0] == 'Destination' or prediction[0] == 'Departure':
		value = org
		station_code = None
		
		# check if location is valid
		for token in doc:
			if token.text in vocab:
				value = token.text
				station_code = KB.get_station_code(value.lower())
		
		
		value = station_code
		
		if value == None:
			label = "Junk"
			value = "Station Not Valid"

	
	if max(scores_list) < 0.2 and label_detected is False:
		labels.append(("Junk", value))
	elif max(scores_list) > 0.2 and max(scores_list) < 0.4 and label_detected is False:
		labels.append(("Possible Junk", prediction[0]))
	elif max(scores_list) > 0.4:
		labels.append((label, value))


	return labels





