year_num_dict = {"January" : 1, "February" : 2, "March" : 3, "April" : 4, "May" : 5, "June" : 6, "July" : 7, "August" : 8, "September" : 9, "October" : 10, "November" : 11, "December" : 12}
year_dict = {'jan': 'January', 'feb': 'February', 'mar': 'March', 'apr': 'April', 'may': 'May', 'jun': 'June', 'jul': 'July', 'aug': 'August', 'sep': 'September', 'oct': 'October', 'nov': 'November', 'dec': 'December', 'january': 'January', 'february': 'February', 'march': 'March', 'april': 'April', 'june': 'June', 'july': 'July', 'august': 'August', 'september': 'September', 'october': 'October', 'november': 'November', 'december': 'December'}

location_code_dict = {}
location_dict = {}

def dict_year_num(str):
	return year_num_dict[str]
def location(loc):
	return location_dict[loc]

def get_year_dict():
	return list(year_dict)
	
def update_year_dict(code, month):
	year_dict[code] = month

def update_location_dict(other_loc, location):
	location_dict[other_loc] = location

def build_location_code_dict():
	import csv
	with open('NLP/stations.csv', newline='') as csvfile:
		training = csv.DictReader(csvfile)
		for row in training:
			location_code_dict[row['name'].lower()] = row['tiploc']
			
			# build location dictionary
			location_dict[row['name']] = row['name'].lower()
			location_dict[row['name'].lower()] = row['name'].lower()
			location_dict[row['name'].capitalize()] = row['name'].lower()
			


def get_station_code(station):
	# NEEDS TO BE INITIALIZED
	build_location_code_dict()
	code = None
	
	try:
		code = location_code_dict[station] 
	except KeyError:
		pass
		
	return code

def get_stations():
	build_location_code_dict()
	return list(location_dict)


#print(get_stations())


